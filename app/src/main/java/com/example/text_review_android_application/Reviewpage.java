package com.example.text_review_android_application;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONObject;

import java.util.HashMap;

public class Reviewpage extends AppCompatActivity {

    private Button bcake,bbiryani,bburger,bchickendry,bdosa,bicecream,bjuice,bgobi,bpaneer,bpizza,bvegbiryani;
    private TextInputLayout tcake,tbiryani,tburger,tchickendry,tdosa,ticecream,tjuice,tgobi,tpaneer,tpizza,tvegbiryani;
    private ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reviewpage);

        progressDialog = new ProgressDialog(this);
        bcake=findViewById(R.id.bcake);
        tcake = findViewById(R.id.tcake);

        bbiryani=findViewById(R.id.bbiryani);
        tbiryani=findViewById(R.id.tbiryani);

        bburger=findViewById(R.id.bburger);
        tburger=findViewById(R.id.tburger);

        bchickendry= findViewById(R.id.bchickendry);
        tchickendry=findViewById(R.id.tchickendry);

        bdosa=findViewById(R.id.bdosa);
        tdosa=findViewById(R.id.tdosa);

        bicecream=findViewById(R.id.bicecream);
        ticecream=findViewById(R.id.ticecream);

        bjuice=findViewById(R.id.bjuice);
        tjuice=findViewById(R.id.tjuice);

        bgobi=findViewById(R.id.bgobi);
        tgobi=findViewById(R.id.tgobi);

        bpaneer=findViewById(R.id.bpaneer);
        tpaneer=findViewById(R.id.tpaneer);

        bpizza=findViewById(R.id.bpizza);
        tpizza=findViewById(R.id.tpizza);

        bvegbiryani=findViewById(R.id.bvegbiryani);
        tvegbiryani=findViewById(R.id.tvegbiryani);

        bcake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String itemid = "ck_01";
                String itemname = "Cake";
                String itemreview = tcake.getEditText().getText().toString().trim();
                tcake.setError(null);
                if(TextUtils.isEmpty(itemid)) {
                    tcake.setError("Enter Review");
                }
                else {
                    progressDialog.setMessage("Uploading");
                    progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progressDialog.show();

                    HashMap<String,String> reviewdata = new HashMap<>();
                    reviewdata.put("id", itemid);
                    reviewdata.put("item_name", itemname);
                    reviewdata.put("item_review", itemreview);

                    postdata(reviewdata);

                }

            }
        });

        bbiryani.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String itemid = "br_01";
                String itemname = "Biryani";
                String itemreview = tbiryani.getEditText().getText().toString().trim();
                tbiryani.setError(null);
                if(TextUtils.isEmpty(itemid)) {
                    tbiryani.setError("Enter Review");
                }
                else {
                    progressDialog.setMessage("Uploading");
                    progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progressDialog.show();

                    HashMap<String,String> reviewdata = new HashMap<>();
                    reviewdata.put("id", itemid);
                    reviewdata.put("item_name", itemname);
                    reviewdata.put("item_review", itemreview);

                    postdata(reviewdata);

                }
            }
        });

        bburger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String itemid = "br_02";
                String itemname = "Burger";
                String itemreview = tburger.getEditText().getText().toString().trim();
                tburger.setError(null);
                if(TextUtils.isEmpty(itemid)) {
                    tburger.setError("Enter Review");
                }
                else {
                    progressDialog.setMessage("Uploading");
                    progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progressDialog.show();

                    HashMap<String,String> reviewdata = new HashMap<>();
                    reviewdata.put("id", itemid);
                    reviewdata.put("item_name", itemname);
                    reviewdata.put("item_review", itemreview);

                    postdata(reviewdata);

                }
            }
        });

        bchickendry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String itemid = "Ck_02";
                String itemname = "Chicken Dry";
                String itemreview = tchickendry.getEditText().getText().toString().trim();
                tchickendry.setError(null);
                if(TextUtils.isEmpty(itemid)) {
                    tchickendry.setError("Enter Review");
                }
                else {
                    progressDialog.setMessage("Uploading");
                    progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progressDialog.show();

                    HashMap<String,String> reviewdata = new HashMap<>();
                    reviewdata.put("id", itemid);
                    reviewdata.put("item_name", itemname);
                    reviewdata.put("item_review", itemreview);

                    postdata(reviewdata);

                }
            }
        });

        bdosa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String itemid = "Do_02";
                String itemname = "Dosa";
                String itemreview = tdosa.getEditText().getText().toString().trim();
                tdosa.setError(null);
                if(TextUtils.isEmpty(itemid)) {
                    tdosa.setError("Enter Review");
                }
                else {
                    progressDialog.setMessage("Uploading");
                    progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progressDialog.show();

                    HashMap<String,String> reviewdata = new HashMap<>();
                    reviewdata.put("id", itemid);
                    reviewdata.put("item_name", itemname);
                    reviewdata.put("item_review", itemreview);

                    postdata(reviewdata);

                }
            }
        });

        bicecream.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String itemid = "IC_01";
                String itemname = "Ice Cream";
                String itemreview = ticecream.getEditText().getText().toString().trim();
                ticecream.setError(null);
                if(TextUtils.isEmpty(itemid)) {
                    ticecream.setError("Enter Review");
                }
                else {
                    progressDialog.setMessage("Uploading");
                    progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progressDialog.show();

                    HashMap<String,String> reviewdata = new HashMap<>();
                    reviewdata.put("id", itemid);
                    reviewdata.put("item_name", itemname);
                    reviewdata.put("item_review", itemreview);

                    postdata(reviewdata);

                }
            }
        });

        bjuice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String itemid = "JC_01";
                String itemname = "Juice";
                String itemreview = tjuice.getEditText().getText().toString().trim();
                tjuice.setError(null);
                if(TextUtils.isEmpty(itemid)) {
                    tjuice.setError("Enter Review");
                }
                else {
                    progressDialog.setMessage("Uploading");
                    progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progressDialog.show();

                    HashMap<String,String> reviewdata = new HashMap<>();
                    reviewdata.put("id", itemid);
                    reviewdata.put("item_name", itemname);
                    reviewdata.put("item_review", itemreview);

                    postdata(reviewdata);

                }
            }
        });

        bgobi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String itemid = "Gb_01";
                String itemname = "Gobi Dry";
                String itemreview = tgobi.getEditText().getText().toString().trim();
                tgobi.setError(null);
                if(TextUtils.isEmpty(itemid)) {
                    tgobi.setError("Enter Review");
                }
                else {
                    progressDialog.setMessage("Uploading");
                    progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progressDialog.show();

                    HashMap<String,String> reviewdata = new HashMap<>();
                    reviewdata.put("id", itemid);
                    reviewdata.put("item_name", itemname);
                    reviewdata.put("item_review", itemreview);

                    postdata(reviewdata);

                }
            }
        });

        bpaneer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String itemid = "Pn_01";
                String itemname = "Panner Chilly";
                String itemreview = tpaneer.getEditText().getText().toString().trim();
                tpaneer.setError(null);
                if(TextUtils.isEmpty(itemid)) {
                    tpaneer.setError("Enter Review");
                }
                else {
                    progressDialog.setMessage("Uploading");
                    progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progressDialog.show();

                    HashMap<String,String> reviewdata = new HashMap<>();
                    reviewdata.put("id", itemid);
                    reviewdata.put("item_name", itemname);
                    reviewdata.put("item_review", itemreview);

                    postdata(reviewdata);

                }
            }
        });

        bpizza.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String itemid = "Pz_01";
                String itemname = "Pizza";
                String itemreview = tpizza.getEditText().getText().toString().trim();
                tpizza.setError(null);
                if(TextUtils.isEmpty(itemid)) {
                    tpizza.setError("Enter Review");
                }
                else {
                    progressDialog.setMessage("Uploading");
                    progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progressDialog.show();

                    HashMap<String,String> reviewdata = new HashMap<>();
                    reviewdata.put("id", itemid);
                    reviewdata.put("item_name", itemname);
                    reviewdata.put("item_review", itemreview);

                    postdata(reviewdata);

                }
            }
        });

        bvegbiryani.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String itemid = "Vg_01";
                String itemname = "Veg  Fried Rice";
                String itemreview = tvegbiryani.getEditText().getText().toString().trim();
                tvegbiryani.setError(null);
                if(TextUtils.isEmpty(itemid)) {
                    tvegbiryani.setError("Enter Review");
                }
                else {
                    progressDialog.setMessage("Uploading");
                    progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progressDialog.show();

                    HashMap<String,String> reviewdata = new HashMap<>();
                    reviewdata.put("id", itemid);
                    reviewdata.put("item_name", itemname);
                    reviewdata.put("item_review", itemreview);

                    postdata(reviewdata);

                }
            }
        });
    }

    private void postdata(HashMap<String, String> reviewdata) {

        String url = "https://review-text-analysis.herokuapp.com/reviews";
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(reviewdata), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                progressDialog.dismiss();
                Log.i("check", response.toString());
                Toast.makeText(getApplicationContext(),"Recieved..!",Toast.LENGTH_SHORT).show();
                nextpage();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Log.i("error",error.toString());
                Toast.makeText(getApplicationContext(),"Failed Try Again Later..!",Toast.LENGTH_SHORT).show();
                nextpage();
            }
        });

        requestQueue.add(jsonObjectRequest);
    }

    private void nextpage() {
        Intent intent= new Intent(this,MainActivity.class);
        startActivity(intent);
    }
}