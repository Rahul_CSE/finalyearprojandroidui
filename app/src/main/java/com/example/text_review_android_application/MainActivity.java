package com.example.text_review_android_application;

import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button b1,b2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        b1=findViewById(R.id.bcustom);
        b2=findViewById(R.id.breview);
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openNewActivity();
            }
        });

        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openNewActivity2();
            }
        });
    }

    private void openNewActivity2() {
        Intent intent = new Intent(this,Reviewpage.class);
        startActivity(intent);
    }

    private void openNewActivity() {
        Intent intent = new Intent(this,Customreview.class);
        startActivity(intent);
    }
}