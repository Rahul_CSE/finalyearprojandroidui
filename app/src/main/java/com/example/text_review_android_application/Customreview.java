package com.example.text_review_android_application;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import okhttp3.internal.Util;

public class Customreview extends AppCompatActivity {

    private Button bsub;
    private TextInputLayout tid,titem,treview;
    private ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customreview);

        bsub=findViewById(R.id.bsubmitcustomreview);
        tid = findViewById(R.id.tid);
        titem = findViewById(R.id.titem);
        treview = findViewById(R.id.titemreview);

        progressDialog = new ProgressDialog(this);

        bsub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String itemid = tid.getEditText().getText().toString().trim();
                String itemname = titem.getEditText().getText().toString().trim();
                String itemreview = treview.getEditText().getText().toString().trim();
                tid.setError(null);
                titem.setError(null);
                treview.setError(null);
                if (TextUtils.isEmpty(itemid)) {
                    tid.setError("Need to fill this");
                } else if (TextUtils.isEmpty(itemname)) {
                    titem.setError("Need to fill this");
                } else if (TextUtils.isEmpty((itemreview))) {
                    treview.setError("Need to fill this");
                } else {
                    progressDialog.setMessage("Uploading");
                    progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progressDialog.show();

                    Log.i("work","workked");
                    HashMap<String,String> reviewdata = new HashMap<>();
                        reviewdata.put("id", itemid);
                        reviewdata.put("item_name", itemname);
                        reviewdata.put("item_review", itemreview);
                    Log.i("hey", reviewdata.toString());

                    String url = "https://review-text-analysis.herokuapp.com/reviews";
                    RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                    JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(reviewdata), new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            progressDialog.dismiss();
                            Log.i("check", response.toString());
                            Toast.makeText(getApplicationContext(),"Recieved..!",Toast.LENGTH_SHORT).show();
                            nextpage();
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                            Log.i("error",error.toString());
                            Toast.makeText(getApplicationContext(),"Failed Try Again..!",Toast.LENGTH_SHORT).show();
                        }
                    });

                    requestQueue.add(jsonObjectRequest);
                }
            }
        });
    }

    private void nextpage() {

        Intent intent = new Intent(this,MainActivity.class);
        startActivity(intent);
    }

}